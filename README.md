# Practica 4 - Spark Structured Streaming - Joins and late data processing

## Ejercicio TotalCallSecondsStreaming

### Objetivo

El objetivo de este ejercicio es calcular a partir de los registros de llamadas completas obtenidos
 desde el topic `streaming.network.call_summary`, las mismas estadisticas de llamadas de la practica 0b
 pero en este caso calculado en streaming. Para ello debemos generar registros con los datos de la duracion agregada 
 de las llamadas realizadas agrupadas por Provincia y Edad. 
 

Los registros en el topic de origen son de resumen de llamadas (call_summaries) en formato JSON, y siguen este esquema:


|Field|Type|Description|
|---|---|---|
|initialTime|Timestamp| Timestamp when the call started|
|endTime|Timestamp| Timestamp when the call finished|
|originatingMsisdn| String| The phone number of the caller| 
|terminatingMsisdn| String| The phone number of the callee| 
|originatingCell| String| The cell id of the caller| 
|terminatingCell| String| The cell id of the callee| 


El esquema de los registros generados debe ser el siguiente. La codificacion será JSON:

| Field | Type | Description |
|---------|------|------|
| province | String | Province where the call starts |
| age | Int | Caller age |
| total_seconds | Long | Total accumulate call duration in seconds |


## Ejercicio CallsPerCityPerPeriod

### Objetivo

El objetivo de este ejercicio es escribir una aplicación de streaming que procese los eventos de red desde el topic 
`streaming.network.signalling`, los cruce con información estática de topología de red y calcule el número de llamadas
por minuto realizadas desde la provincia proporcionada por linea de comando, clasificadas por ciudades

El esquema de los eventos de red, codificados en formato json, es el siguiente:

|Field|Type|Description|
|---|---|---|
|timestamp|Timestamp| The timestamp of the record|
|msisdn| String| The phone number calling or being called| 
|callId| String| An identifier of the call, shared among all records|
|outgoingCall| Boolean| A flag specifying the call direction
|cell| String| The cell id this msisdn is attached to
|eventType| Int| 0: call attempt, 1: call established, 2: call dropped, 3: call finished
|conversationTime| Int | For call established segments, the duration of the call

Un ejemplo real de registro en el topic:
```bash
{"timestamp": "2019-04-09 16:29:11.795", "msisdn": "600000063", "call_id": "581d2851-5528-4baa-9316-8432d04d736f", "outgoing_call": false, "cell": "cell-00691", "event_type": 0, "conversation_time": 0}
```

El esquema del CSV que contiene la topologia de red es el siguiente:

|Field|Type|Description|
|---|---|---|
|cell_id| String| The cell ID of this entry|
|city| String| The city this cell is located in | 
|province| String| The province this cell is located in|

Las llamadas de teléfono se codifican de la siguiente manera, asumiendo que A es el llamante y B es el llamado.

1. Cuando la llamada se inicia, se registra un evento de tipo 0 (_call attempt_) para A y para B, con el flag 
`outgoingCall` establecido apropiadamente en cada caso
2. Una vez que la llamada está en curso, se registran eventos de tipo 1 (_call established_) periódicamente para A y para B,
cada uno de ellos incluyendo el tiempo de conversación. Cada llamada típicamente incluye varios registros de este tipo.
3. Cuando la llamada finaliza, se registra un evento de tipo 3 (_call finished_) para A y para B.

El sink de este pipeline será un fichero parquet de HDFS con el formato siguiente (solo incluimos el inicio de ventana como
timestamp):

| time (string)    | city (string) |  count |
|------------------|---------------|--------|
| 2019-03-19 19:10 | Madrid        |  2     |
| 2019-03-19 19:10 | Mostoles      |  3     |


El pipeline de transformación se codificará empleando únicamente el API de Spark SQL (Dataframes + funciones sql)

La referencia de estas funciones se puede encontrar en el siguiente enlace de la documentación oficial:

https://spark.apache.org/docs/2.3.1/api/scala/index.html#org.apache.spark.sql.functions$

#### Gestión de late data

Ejecuta la aplicación y compara la tabla de agregados generada con una tabla calculada en batch sobre los datos
generados para ver como afectan los eventos tardíos a los cálculos.

Recompila la aplicación con un valor de watermark de 3 minutos y vuelve a ejecutarla y compara los resultados ahora.

### Compilación y empaquetado

Para compilar y empaquetar la aplicación, ejecutar desde este mismo directorio el siguiente comando:

```bash
mvn clean package
```

También se puede ejecutar la tarea Maven desde el IDE (vista Maven -> Lifecycle -> package)

### Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-joins-0.1.0-SNAPSHOT.jar edge:

spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G --conf spark.sql.shuffle.partitions=10 \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.CallsPerCityPerPeriod \
spark-structured-joins-0.1.0-SNAPSHOT.jar <originating call province>
```


## Ejercicio CallsPerCityPerPeriodDataset

Reimplementa el ejercicio anterior usando el API de Datasets en vez de emplear el de Dataframes.

Se proporcionan las case clases correspondientes a los modelos de evento de red y celda.

### Ejecución

El proyecto está configurado para generar un _fat jar_ ejecutable bajo el directorio `target`. Para ejecutarlo, copiarlo 
al nodo edge01 y utilizar `spark2-submit` de la siguiente manera:

```bash
scp target/spark-structured-joins-0.1.0-SNAPSHOT.jar edge:

spark2-submit --master yarn --num-executors 2 --executor-cores 2 --executor-memory 1G --conf spark.sql.shuffle.partitions=10 \
--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0  --class edu.comillas.mbd.streaming.spark.practices.CallsPerCityPerPeriodDataset \
spark-structured-joins-0.1.0-SNAPSHOT.jar <originating call province>
``` 
