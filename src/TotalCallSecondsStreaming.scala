import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}


/**
  * A Spark Structured Streaming application to calculate the aggregate call duration per province and age
  */
object TotalCallSecondsStreaming {

  private val logger = Logger.getLogger(getClass.getName)

  // TODO set your user name here
  val user: String = "aalvarez"

  val InputTopic = "streaming.network.call_summary"
  val TopologyCsvPath = "/user/dmnavarro/streaming/practice-4/topology.csv"
  val ClientsCsvPath = "/user/dmnavarro/streaming/practice-4/clients.csv"
  val OutputTopic = s"streaming.alumnos.$user.network"

  logger.info(s"Sending output to topic $OutputTopic")

  private val spark = SparkSession.builder()
    .appName(s"TotalCallSecondsStreaming - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._

  val clientsSchema = StructType(
    List(
      StructField("msisdn", StringType, nullable = false),
      StructField("name", StringType, nullable = false),
      StructField("gender", StringType, nullable = false),
      StructField("age", StringType, nullable = false),
      StructField("city", StringType, nullable = false),
      StructField("province", StringType, nullable = false)
    )
  )

  val topologySchema = StructType(
    List(
      StructField("cell_id", StringType, nullable = false),
      StructField("city", StringType, nullable = false),
      StructField("province", StringType, nullable = false)
    )
  )

  val callSummarySchema = StructType(
    List(
      StructField("initialTime", TimestampType),
      StructField("endTime", TimestampType),
      StructField("originatingCell", StringType),
      StructField("originatingMsisdn", StringType),
      StructField("terminatingCell", StringType),
      StructField("terminatingMsisdn", StringType)
    )
  )

  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): DataFrame = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "earliest")
      .load()
      .select('timestamp, 'value cast StringType)
      .select('timestamp, from_json('value, callSummarySchema) as 'call_summary_struct)
      .select(
        'timestamp,
        to_timestamp('call_summary_struct.getField("initialTime"), "yyyy-MM-dd HH:mm:ss.SSS") as 'initialTime,
        to_timestamp('call_summary_struct.getField("endTime"), "yyyy-MM-dd HH:mm:ss.SSS") as 'endTime,
        'call_summary_struct.getField("originatingCell") as 'originatingCell,
        'call_summary_struct.getField("originatingMsisdn") as 'originatingMsisdn,
        'call_summary_struct.getField("terminatingCell") as 'terminatingCell,
        'call_summary_struct.getField("terminatingMsisdn") as 'terminatingMsisdn)
      .withWatermark("timestamp", "0 minutes")
  }


  def main(args: Array[String]): Unit = {
    require(args.length == 1, "CallsPerCityPerPeriod <originating province>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")

    val inputTable = getInputTableFromTopic(InputTopic)

    val topologyTable = spark.read.schema(topologySchema).option("header", true).csv(TopologyCsvPath)
    val clientsTable = spark.read.schema(clientsSchema).option("header", true).csv(ClientsCsvPath)

    // TODO implement this (remember to format it as expected by a Kafka sink)
    val resultTable:DataFrame =
      inputTable
          .withColumn("total_seconds", (unix_timestamp('endTime) - unix_timestamp('initialTime)).cast(LongType))
          .join(topologyTable, col("originatingCell") === col("cell_id"), "inner")
          .join(clientsTable.drop('province), col("originatingMsisdn") === col("msisdn"), "inner")
          .groupBy('province, 'age).agg(sum('total_seconds) as "total_seconds")
          .withColumn("struct", struct('province, 'age, 'total_seconds))
          .select(to_json('struct) as "value")


    val streamingQuery = resultTable
      .writeStream
      .outputMode(OutputMode.Update)
      .format("kafka")
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("topic", OutputTopic)
      .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/summaries")
      .trigger(Trigger.ProcessingTime("1 second"))
      .start()

    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()

  }
}