import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

object CallsPerCityPerPeriod {

  // TODO set your user name here
  val user: String = "aalvarez"

  // TODO change this if needed
  val destinationTable: String = s"/user/$user/streaming/callspercity"

  val InputTopic = "streaming.network.signalling"
  val TopologyCsvPath = "/user/dmnavarro/streaming/practice-4/topology.csv"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"CallsPerCityPerPeriod - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._


  val networkRecordSchema = StructType(
    List(
      StructField("timestamp", TimestampType, nullable = false),
      StructField("msisdn", StringType, nullable = false),
      StructField("call_id", StringType, nullable = false),
      StructField("outgoing_call", BooleanType, nullable = false),
      StructField("cell", StringType, nullable = false),
      StructField("event_type", IntegerType, nullable = false),
      StructField("conversation_time", IntegerType, nullable = false)
    )
  )

  val topologySchema = StructType(
    List(
      StructField("cell_id", StringType, nullable = false),
      StructField("city", StringType, nullable = false),
      StructField("province", StringType, nullable = false)
    )
  )
  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): DataFrame = {
    spark.readStream
        .format("kafka")
        .option("subscribe", kafkaTopic)
        .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
        .option("startingOffsets", "earliest")
        .load()
        .select('value cast StringType)
        .select(from_json('value, networkRecordSchema) as 'network_record_struct)
        .select(to_timestamp('network_record_struct.getField("timestamp"), "yyyy-MM-dd HH:mm:ss.SSS") as 'timestamp,
          'network_record_struct.getField("msisdn") as 'msisdn,
          'network_record_struct.getField("call_id") as 'call_id,
          'network_record_struct.getField("outgoing_call") as 'outgoing_call,
          'network_record_struct.getField("cell") as 'cell,
          'network_record_struct.getField("event_type") cast IntegerType as 'event_type,
          'network_record_struct.getField("conversation_time") cast IntegerType as 'conversation_time)
        .withWatermark("timestamp", "0 minutes")
  }


  def main(args: Array[String]): Unit = {
    require(args.length == 1, "CallsPerCityPerPeriod <originating province>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")

    val province = args(0)

    val inputTable = getInputTableFromTopic(InputTopic)

    val topologyTable = spark.read.schema(topologySchema).option("header", true).csv(TopologyCsvPath)

    // TODO implement this
    val resultTable:DataFrame =
      inputTable
        .filter('outgoing_call === true)
        .join(topologyTable, col("cell") === col("cell_id"), "inner")
        .withWatermark("timestamp", "0 minutes")
        .groupBy( window('timestamp, "1 minute"), 'city)
        .agg(count('call_id) as "count")
        .select(to_json(struct('time, 'city, 'count)) as "value")

    val streamingQuery = resultTable
        .writeStream
        .outputMode(OutputMode.Append)
        .format("parquet")
        .option("path", destinationTable)
        .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/joins/df")
        .trigger(Trigger.ProcessingTime("5 seconds"))
        .start()

    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()


  }
}