import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout, OutputMode, Trigger}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

case class NetworkRecord(timestamp: java.sql.Timestamp,
                         msisdn: String,
                         callId: String,
                         outgoingCall: Boolean,
                         cell: String,
                         eventType: Int,
                         conversationTime: Int)

case class Cell(cell_id: String,
                city: String,
                province: String)


case class CallsPerCityPerMinute(time: String,
                                 city: String,
                                 count: Long)


object CallsPerCityPerPeriodDataset {

  // TODO set your user name here
  val user: String = "aalvarez"

  // TODO change this if needed
  val destinationTable: String = s"/user/$user/streaming/callspercity_dataset"

  val InputTopic = "streaming.network.signalling"
  val TopologyCsvPath = "/user/dmnavarro/streaming/practice-4/topology.csv"

  private val logger = Logger.getLogger(getClass.getName)

  private val spark = SparkSession.builder()
    .appName(s"CallsPerCityPerPeriodDataset - $user")
    .master("yarn")
    .getOrCreate()

  // this is needed to use symbols as column names
  import spark.implicits._


  val networkRecordSchema = StructType(
    List(
      StructField("timestamp", TimestampType, nullable = false),
      StructField("msisdn", StringType, nullable = false),
      StructField("call_id", StringType, nullable = false),
      StructField("outgoing_call", BooleanType, nullable = false),
      StructField("cell", StringType, nullable = false),
      StructField("event_type", IntegerType, nullable = false),
      StructField("conversation_time", IntegerType, nullable = false)
    )
  )

  val topologySchema = StructType(
    List(
      StructField("cell_id", StringType, nullable = false),
      StructField("city", StringType, nullable = false),
      StructField("province", StringType, nullable = false)
    )
  )

  /**
    * Returns a streaming dataframe from the specified topic
    * @param kafkaTopic
    * @return
    */
  def getInputTableFromTopic(kafkaTopic: String): Dataset[NetworkRecord] = {
    spark.readStream
      .format("kafka")
      .option("subscribe", kafkaTopic)
      .option("kafka.bootstrap.servers", "master01.bigdata.alumnos.upcont.es:9092")
      .option("startingOffsets", "earliest")
      .load()
      .select('value cast StringType)
      .select(from_json('value, networkRecordSchema) as 'network_record_struct)
      .select(to_timestamp('network_record_struct.getField("timestamp"), "yyyy-MM-dd HH:mm:ss.SSS") as 'timestamp,
        'network_record_struct.getField("msisdn") as 'msisdn,
        'network_record_struct.getField("call_id") as 'callId,
        'network_record_struct.getField("outgoing_call") as 'outgoingCall,
        'network_record_struct.getField("cell") as 'cell,
        'network_record_struct.getField("event_type") cast IntegerType as 'eventType,
        'network_record_struct.getField("conversation_time") cast IntegerType as 'conversationTime)
      .as[NetworkRecord]
  }


  def mappingFunction(key: (String, String), values: Iterator[(NetworkRecord, Cell)], state: GroupState[CallsPerCityPerMinute]): Iterator[CallsPerCityPerMinute] = {

    var result:Iterator[CallsPerCityPerMinute] = null

    if(state.hasTimedOut){
      result = Iterator[CallsPerCityPerMinute](state.get)
      state.remove()

    }else if (state.exists){
      val existingState = state.get
      val newState = CallsPerCityPerMinute(key._1, key._2, existingState.count + values.size)
      state.update(newState)
      result = Iterator[CallsPerCityPerMinute](newState)

    }else{
      val initialState = CallsPerCityPerMinute(key._1, key._2, values.size)
      state.update(initialState)
      state.setTimeoutDuration("1 minute")
      result = Iterator[CallsPerCityPerMinute](initialState)
    }

    result
  }

  def main(args: Array[String]): Unit = {
    require(args.length == 1, "CallsPerCityPerPeriodDataset <originating province>")
    logger.info(s"Creating Spark Structured Streaming input table from topic $InputTopic")

    val province = args(0)

    val inputTable: Dataset[NetworkRecord] = getInputTableFromTopic(InputTopic)
    val topologyTable: Dataset[Cell] = spark.read.schema(topologySchema)
      .option("header", true)
      .option("ignoreLeadingWhiteSpace", true)
      .option("ignoreTrailingWhiteSpace", true)
      .csv(TopologyCsvPath)
      .as[Cell]

    // TODO implement this
    val resultTable: Dataset[CallsPerCityPerMinute] =
      inputTable.filter(r => r.outgoingCall)
          .joinWith(topologyTable, col("cell") === col("cell_id"), "inner")
          .groupByKey(r => (r._1.timestamp.formatted("yyyy-MM-dd HH:mm"), r._2.city))
          .flatMapGroupsWithState(OutputMode.Append(), GroupStateTimeout.ProcessingTimeTimeout())(mappingFunction)

    val streamingQuery = resultTable
      .writeStream
      .outputMode(OutputMode.Append)
      .format("parquet")
      .option("path", destinationTable)
      .option("checkpointLocation", s"/tmp/spark/streaming/checkpoint/$user/joins/ds")
      .trigger(Trigger.ProcessingTime("5 seconds"))
      .start()

    logger.info(s"Streaming query ${streamingQuery.name} started!")
    streamingQuery.awaitTermination()

  }
}